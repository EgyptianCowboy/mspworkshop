#include <msp430.h>
#include <stdint.h>

void    initSwitch(void);
uint8_t readSwitch(void);

int main (void) {
    uint8_t lastRead = 1;
    uint8_t val;
    uint64_t i;

    WDTCTL = WDTPW + WDTHOLD;

    P1OUT = 0x00;
    P2OUT = 0x00;
    P1DIR = 0x40;
    initSwitch();

    while(1) {
        val = readSwitch();

        if(val != lastRead) {
            for(i=0; i < 1000; i++);

            val = readSwitch();

            if(val != lastRead) {
                lastRead = val;

                if(val==0) {
                    P1OUT ^= 0x40;
                }
            }
        }
    }
    return 0;
}

void initSwitch(void) {
    P1DIR &= 0xF7;
    P1OUT |= 0x08;
    P1REN |= 0x08;
}

uint8_t readSwitch(void) {
    if(P1IN & 0x08) {
        return 1;
    } else {
        return 0;
    }
}
