#include <msp430g2553.h>
#include <stdint.h>
#include <string.h>

// Matrix display constants
#define MTXDIR P1DIR
#define MTXOUT P1OUT
#define MTXDATA BIT7
#define MTXCLK BIT5
#define MTXLATCH BIT4

#define ROW1 BIT0
#define ROW2 BIT1
#define ROW3 BIT2
#define ROW4 BIT3

#define COL1 BIT6 // XIN
#define COL2 BIT5
#define COL3 BIT4

#define ROWCNT 8

void initMtx(void);
void lightMtx(void);

void initKeypad(void);
uint8_t scanKeypadDBC(void);
uint8_t scanKeypad(void);

//void setPattern(uint8_t letter, uint8_t pos);
void setPattern(uint8_t player, uint8_t keypad);

uint8_t winFunc();
void setGreen();
void setRed();
void setBlue();

unsigned char empty[24] = {0x00,0x00,0x00,
                           0x00,0x00,0x00,
                           0x00,0x00,0x00,
                           0x00,0x00,0x00,
                           0x00,0x00,0x00,
                           0x00,0x00,0x00,
                           0x00,0x00,0x00,
                           0x00,0x00,0x00};

//                          B    G    R
unsigned char full[24] =  {0x24,0x00,0x00,
                           0x24,0x00,0x00,
                           0xFF,0x00,0x00,
                           0x24,0x00,0x00,
                           0x24,0x00,0x00,
                           0xFF,0x00,0x00,
                           0x24,0x00,0x00,
                           0x24,0x00,0x00};

unsigned char pattern[24] = {0};

// Invalid move is square is taken or board is full, if player 1 presses -> X, player 2 -> O
uint8_t resetboard[10] = {' ', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
uint8_t board[10] = {' ' , '1', '2', '3', '4', '5', '6', '7', '8', '9'};

int main() {
    WDTCTL = WDTPW | WDTHOLD;
    
    uint8_t playerFlag = 0;
    uint8_t winflag = 0;
    uint8_t moveCount = 0;
    
    initKeypad();
    initMtx();
    initKeypad();
    volatile uint32_t x = 0;
    
    memcpy(pattern, full, sizeof(pattern));
    uint8_t keypadBuffer = 0;
    
    while(1) {
        while((winflag = winFunc())) {
            lightMtx();
            if(scanKeypadDBC() == 10) {
                memcpy(board, resetboard, sizeof(board));
                memcpy(pattern, full, sizeof(pattern));
                moveCount = 0;
                winflag = 0;
            }
        }

        while(moveCount == 9) {
            setBlue();
            lightMtx();
            for(x = 0; x <= 30000; x++);
            memcpy(board, resetboard, sizeof(board));
            memcpy(pattern, full, sizeof(pattern));
            moveCount = 0;
            x = 0;
        }
        
        if((keypadBuffer = scanKeypadDBC())) {
            if(keypadBuffer == 10) {
                memcpy(board, resetboard, sizeof(board));
                memcpy(pattern, full, sizeof(pattern));
                moveCount = 0;
            }
            if(keypadBuffer > 0 && keypadBuffer < 10 ) {
                if((board[keypadBuffer] == 'R') || (board[keypadBuffer] == 'G')) {
                    //skip
                } else {
                    setPattern(playerFlag, keypadBuffer);
                    if(playerFlag) {
                        board[keypadBuffer] = 'R';
                    } else {
                        board[keypadBuffer] = 'G';
                    }
                    playerFlag ^= 0x01;
                    moveCount++;
                }
            }
            keypadBuffer = 0;
        }
        lightMtx();
    }
}

void initMtx(void) {
    // Initialise pin that controls the Matrix Display
    MTXDIR |= MTXDATA + MTXCLK + MTXLATCH; // make outputs
    MTXOUT &= ~(MTXDATA + MTXCLK); // make low
    MTXOUT |= MTXLATCH; // LATCH is active low
}

void lightMtx(void) {
    // Sends each byte in pattern buffer from byte 23 down to byte 0 to the matrix display module
    // Each byte is sent with the LSB first
    int x, by;
    unsigned char val;
    
    MTXOUT &= ~MTXLATCH; // make latch line low
    
    for( by=23 ; by>=0 ; by-- ) {
        val = pattern[by]; // load it into val
        
        for( x=8 ; x>0 ; x-- ) {
            if( val&BIT0 ) {
                MTXOUT |= MTXDATA; // make data line high
            } else {
                MTXOUT &= ~MTXDATA; // make data line low
            }
            
            MTXOUT |= MTXCLK; // make Clk line high
            MTXOUT &= ~MTXCLK; // make Clk line low
            val >>= 1;
        }
    }
    MTXOUT |= MTXLATCH; // make latch line high
}


void initKeypad(void) {
    P2SEL = 0x00;
    P2DIR = 0x00;
    P2OUT = 0x00;
    P2REN = 0xFF;
}

uint8_t scanKeypadDBC(void) {
    static uint8_t lastRead = 16;
    volatile uint8_t dCount;
    uint8_t val;

    val = scanKeypad();

    if(val != lastRead) {
        for (dCount = 0; dCount < 10; dCount++);

        val = scanKeypad();

        if(val != lastRead) {
            lastRead = val;
            return val;
        }
    }
    return 0;
}

uint8_t scanKeypad(void) {
    initKeypad();

    P2DIR |= COL3;
    P2REN &= ~COL3;
    P2OUT |= COL3;

    if(P2IN & ROW1) {
        return 3;
    }
    if(P2IN & ROW2) {
        return 6;
    }
    if(P2IN & ROW3) {
        return 9;
    }
    if(P2IN & ROW4) {
        return 12;
    }

    initKeypad();

    P2DIR |= COL2;
    P2REN &= ~COL2;
    P2OUT |= COL2;

    if(P2IN & ROW1) {
        return 2;
    }
    if(P2IN & ROW2) {
        return 5;
    }
    if(P2IN & ROW3) {
        return 8;
    }
    if(P2IN & ROW4) {
        return 11;
    }

    initKeypad();

    P2DIR |= COL1;
    P2REN &= ~COL1;
    P2OUT |= COL1;

    if(P2IN & ROW1) {
        return 1;
    }
    if(P2IN & ROW2) {
        return 4;
    }
    if(P2IN & ROW3) {
        return 7;
    }
    if(P2IN & ROW4) {
        return 10;
    }

    initKeypad();

    return 16;
}

void setPattern(uint8_t player, uint8_t keypad) {
    uint8_t offset = 0;
    if(player) {
        offset = 1;
    } else {
        offset = 2;
    }
    switch(keypad) {
    case 1:
        pattern[0+offset] |= 0xC0;
        pattern[3+offset] |= 0xC0;
        break;
    case 2:
        pattern[0+offset] |= 0x18;
        pattern[3+offset] |= 0x18;
        break;
    case 3:
        pattern[0+offset] |= 0x03;
        pattern[3+offset] |= 0x03;
        break;
    case 4:
        pattern[9+offset] |= 0xC0;
        pattern[12+offset] |= 0xC0;
        break;
    case 5:
        pattern[9+offset] |= 0x18;
        pattern[12+offset] |= 0x18;
        break;
    case 6:
        pattern[9+offset] |= 0x03;
        pattern[12+offset] |= 0x03;
        break;
    case 7:
        pattern[18+offset] |= 0xC0;
        pattern[21+offset] |= 0xC0;
        break;
    case 8:
        pattern[18+offset] |= 0x18;
        pattern[21+offset] |= 0x18;
        break;
    case 9:
        pattern[18+offset] |= 0x03;
        pattern[21+offset] |= 0x03;
        break;
    default:
        break;
    }
    /*
    if(letter == 'R') {
        switch(pos) {
        case 1:
            pattern[2] |= 0xC0;
            pattern[5] |= 0xC0;
            break;
        case 2:
            pattern[2] |= 0x18;
            pattern[5] |= 0x18;
            break;
        case 3:
            pattern[2] |= 0x03;
            pattern[5] |= 0x03;
            break;
        case 4:
            pattern[11] |= 0xC0;
            pattern[14] |= 0xC0;
            break;
        case 5:
            pattern[11] |= 0x18;
            pattern[14] |= 0x18;
            break;
        case 6:
            pattern[11] |= 0x03;
            pattern[14] |= 0x03;
            break;
        case 7:
            pattern[20] |= 0xC0;
            pattern[23] |= 0xC0;
            break;
        case 8:
            pattern[20] |= 0x18;
            pattern[23] |= 0x18;
            break;
        case 9:
            pattern[20] |= 0x03;
            pattern[23] |= 0x03;
            break;
        default:
            break;
        }
    } else if(letter == 'G') {
        switch(pos) {
        case 1:
            pattern[1] |= 0xC0;
            pattern[4] |= 0xC0;
            break;
        case 2:
            pattern[1] |= 0x18;
            pattern[4] |= 0x18;
            break;
        case 3:
            pattern[1] |= 0x03;
            pattern[4] |= 0x03;
            break;
        case 4:
            pattern[10] |= 0xC0;
            pattern[13] |= 0xC0;
            break;
        case 5:
            pattern[10] |= 0x18;
            pattern[13] |= 0x18;
            break;
        case 6:
            pattern[10] |= 0x03;
            pattern[13] |= 0x03;
            break;
        case 7:
            pattern[19] |= 0xC0;
            pattern[22] |= 0xC0;
            break;
        case 8:
            pattern[19] |= 0x18;
            pattern[22] |= 0x18;
            break;
        case 9:
            pattern[19] |= 0x03;
            pattern[22] |= 0x03;
            break;
        default:
            break;
        }
    }*/    
}

uint8_t winFunc () {
    if((board[1] & board[2] & board[3]) == 'G') {
        setGreen();
        return 1;
    } else if((board[1] & board[4] & board[7]) == 'G') {
        setGreen();
        return 1;
    } else if((board[7] & board[8] & board[9]) == 'G') {
        setGreen();
        return 1;
    } else if((board[3] & board[6] & board[9]) == 'G') {
        setGreen();
        return 1;
    } else if((board[1] & board[5] & board[9]) == 'G') {
        setGreen();
        return 1;
    } else if((board[4] & board[5] & board[6]) == 'G') {
        setGreen();
        return 1;
    } else if((board[3] & board[5] & board[7]) == 'G') {
        setGreen();
        return 1;
    }

    if((board[1] & board[2] & board[3]) == 'R') {
        setRed();
        return 1;
    } else if((board[1] & board[4] & board[7]) == 'R') {
        setRed();
        return 1;
    } else if((board[7] & board[8] & board[9]) == 'R') {
        setRed();
        return 1;
    } else if((board[3] & board[6] & board[9]) == 'R') {
        setRed();
        return 1;
    } else if((board[1] & board[5] & board[9]) == 'R') {
        setRed();
        return 1;
    } else if((board[4] & board[5] & board[6]) == 'R') {
        setRed();
        return 1;
    } else if((board[3] & board[5] & board[7]) == 'R') {
        setRed();
        return 1;
    }
    return 0;
}

void setGreen() {
    memcpy(pattern, empty, sizeof(pattern));
    pattern[1] = 0xFF;
    pattern[4] = 0xFF;
    pattern[7] = 0xFF;
    pattern[10] = 0xFF;
    pattern[13] = 0xFF;
    pattern[16] = 0xFF;
    pattern[19] = 0xFF;
    pattern[22] = 0xFF;
}

void setRed() {
    memcpy(pattern, empty, sizeof(pattern));
    pattern[2] = 0xFF;
    pattern[5] = 0xFF;
    pattern[8] = 0xFF;
    pattern[11] = 0xFF;
    pattern[14] = 0xFF;
    pattern[17] = 0xFF;
    pattern[20] = 0xFF;
    pattern[23] = 0xFF;
}

void setBlue() {
    memcpy(pattern, empty, sizeof(pattern));
    pattern[0] = 0xFF;
    pattern[3] = 0xFF;
    pattern[6] = 0xFF;
    pattern[9] = 0xFF;
    pattern[12] = 0xFF;
    pattern[15] = 0xFF;
    pattern[18] = 0xFF;
    pattern[21] = 0xFF;
}

