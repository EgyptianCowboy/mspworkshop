OBJECTS=blink.o

GCC_DIR =  /home/sil/.local/ti/msp430-gcc/bin
SUPPORT_FILE_DIRECTORY = /home/sil/.local/ti/msp430-gcc/include

DEVICE  = msp430g2553
CC      = msp430-elf-gcc
GDB     = msp430-elf-gdb

CFLAGS = -I $(SUPPORT_FILE_DIRECTORY) -mmcu=$(DEVICE) -O2 -g
LFLAGS = -L $(SUPPORT_FILE_DIRECTORY)

all: ${OBJECTS}
	$(CC) $(CFLAGS) $(LFLAGS) $? -o blink.out

debug: all
	$(GDB) $(DEVICE).out
