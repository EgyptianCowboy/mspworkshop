#include <msp430g2553.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
//#include <time.h>

// Matrix display constants
#define MTXDIR P1DIR
#define MTXOUT P1OUT
#define MTXDATA BIT7
#define MTXCLK BIT5
#define MTXLATCH BIT4

#define ROW1 BIT0
#define ROW2 BIT1
#define ROW3 BIT2
#define ROW4 BIT3

#define COL1 BIT6 // XIN
#define COL2 BIT5
#define COL3 BIT4

void initMtx(void);
void lightMtx(void);

void initKeypad(void);
uint8_t scanKeypadDBC(void);
uint8_t scanKeypad(void);

typedef struct segment {
    uint8_t posX;
    uint8_t posY;
} segment;

typedef struct snakehead {
    int8_t dirs; // 0 is up, 1 left, 2 down, 3 right
    int8_t length; // amount
    int8_t posX;
    int8_t posY;
    struct segment* segArray;
} snakehead;

typedef struct food{
    int8_t posX;
    int8_t posY;
} food;


uint8_t empty[3][8] = {0};
uint8_t board[3][8] = {0};

int main() {
    WDTCTL = WDTPW | WDTHOLD;
    
    initKeypad();
    initMtx();
    initKeypad();
    snakehead snake;
    snake.dirs = 3;
    snake.length = 0;
    snake.posX = 4;
    snake.posY = 4;

    snake.segArray = malloc(sizeof(segment)*(snake.length));

    //time_t t;
    //srand((signed) time(&t));
    srand((signed)1);
    volatile uint8_t xposBuf = 0;
    volatile uint8_t yposBuf = 0;

    for(uint8_t i = 0; i < snake.length; i++) {
        snake.segArray[i].posX = 4;
        snake.segArray[i].posY = 4;
    }

    uint8_t keypadBuffer = 0;

    uint8_t foodFlag = 0;
    food food;
    
    volatile uint16_t timeindex;
    while(1) {
        if(!foodFlag) {
            foodFlag = 1;
            food.posX = rand()%8;
            food.posY = rand()%8;
        }
        
        if((keypadBuffer = scanKeypadDBC())) {
            switch(keypadBuffer) {
            case 2:
                snake.dirs = 2;
                break;
            case 6:
                snake.dirs = 3;
                break;
            case 8:
                snake.dirs = 0;
                break;
            case 4:
                snake.dirs = 1;
                break;
            }
        }
        switch(snake.dirs) {
        case 0:
            snake.posY++;
            break;
        case 1:
            snake.posX++;
            break;
        case 2:
            snake.posY--;
            break;
        case 3:
            snake.posX--;
            break;
        default:
            break;
        }
        
        snake.posY = snake.posY < 0 ? 7 : snake.posY;
        snake.posY = snake.posY > 7 ? 0 : snake.posY;
        snake.posX = snake.posX < 0 ? 7 : snake.posX;
        snake.posX = snake.posX > 7 ? 0 : snake.posX;
        
        memcpy(board, empty, sizeof(board));
        board[0][snake.posY] = 0x01 << snake.posX;

        board[2][food.posY] = 0x01 << food.posX;
        
        yposBuf = snake.posY;
        xposBuf = snake.posX;

        for(uint8_t i = 0; i < snake.length; i++) {
            board[0][yposBuf] |= (0x01 << xposBuf);
            uint8_t tmpY = snake.segArray[i].posY;
            uint8_t tmpX = snake.segArray[i].posX;
            snake.segArray[i].posX = xposBuf;
            snake.segArray[i].posY = yposBuf;
            yposBuf = tmpY;
            xposBuf = tmpX;
        }

        if(snake.posX == food.posX && snake.posY == food.posY) {
            board[2][food.posY] ^= 0x01 << food.posX;
            foodFlag = 0;
            snake.length++;
            free(snake.segArray);
            snake.segArray = malloc(sizeof(segment)*(snake.length));
        }
        
        lightMtx();
        for(timeindex = 0; timeindex <= 15000; timeindex++);
    }
    return 0;
}

void writesnake() {
    
}

void initMtx(void) {
    // Initialise pin that controls the Matrix Display
    MTXDIR |= MTXDATA + MTXCLK + MTXLATCH; // make outputs
    MTXOUT &= ~(MTXDATA + MTXCLK); // make low
    MTXOUT |= MTXLATCH; // LATCH is active low
}

void lightMtx(void) {
    // Sends each byte in pattern buffer from byte 23 down to byte 0 to the matrix display module
    // Each byte is sent with the LSB first
    int x, bx, by;
    unsigned char val;
    
    MTXOUT &= ~MTXLATCH; // make latch line low

    for( by=7 ; by>= 0 ; by-- ) {
        for(bx=2; bx >= 0; bx--) {
            val = board[bx][by]; // load it into val
            
            for( x=8 ; x>0 ; x-- ) {
                if( val&BIT0 ) {
                    MTXOUT |= MTXDATA; // make data line high
                } else {
                    MTXOUT &= ~MTXDATA; // make data line low
                }
            
                MTXOUT |= MTXCLK; // make Clk line high
                MTXOUT &= ~MTXCLK; // make Clk line low
                val >>= 1;
            }
        }
    }
    MTXOUT |= MTXLATCH; // make latch line high
}


void initKeypad(void) {
    P2SEL = 0x00;
    P2DIR = 0x00;
    P2OUT = 0x00;
    P2REN = 0xFF;
}

uint8_t scanKeypadDBC(void) {
    static uint8_t lastRead = 16;
    volatile uint8_t dCount;
    uint8_t val;

    val = scanKeypad();

    if(val != lastRead) {
        for (dCount = 0; dCount < 10; dCount++);

        val = scanKeypad();

        if(val != lastRead) {
            lastRead = val;
            return val;
        }
    }
    return 0;
}

uint8_t scanKeypad(void) {
    initKeypad();

    P2DIR |= COL3;
    P2REN &= ~COL3;
    P2OUT |= COL3;

    if(P2IN & ROW1) {
        return 3;
    }
    if(P2IN & ROW2) {
        return 6;
    }
    if(P2IN & ROW3) {
        return 9;
    }
    if(P2IN & ROW4) {
        return 12;
    }

    initKeypad();

    P2DIR |= COL2;
    P2REN &= ~COL2;
    P2OUT |= COL2;

    if(P2IN & ROW1) {
        return 2;
    }
    if(P2IN & ROW2) {
        return 5;
    }
    if(P2IN & ROW3) {
        return 8;
    }
    if(P2IN & ROW4) {
        return 11;
    }

    initKeypad();

    P2DIR |= COL1;
    P2REN &= ~COL1;
    P2OUT |= COL1;

    if(P2IN & ROW1) {
        return 1;
    }
    if(P2IN & ROW2) {
        return 4;
    }
    if(P2IN & ROW3) {
        return 7;
    }
    if(P2IN & ROW4) {
        return 10;
    }

    initKeypad();

    return 16;
}
