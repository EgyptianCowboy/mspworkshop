//***************************************************************************************
//  MSP430 Blink the LED Demo - Software Toggle P1.0
//
//  Description; Toggle P1.0 by xor'ing P1.0 inside of a software loop.
//  ACLK = n/a, MCLK = SMCLK = default DCO
//
//                MSP430x5xx
//             -----------------
//         /|\|              XIN|-
//          | |                 |
//          --|RST          XOUT|-
//            |                 |
//            |             P1.2|-->LED
//
//  J. Stevenson
//  Texas Instruments, Inc
//  July 2011
//  Built with Code Composer Studio v5
//***************************************************************************************

#include <msp430.h>				

int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	
    P1DIR |= 0x01;
    volatile unsigned int i = 0;
    
    while(1) {
        P1OUT ^= 0x01;
        
        for(i=0; i<=30000; i++) {
        }
    }   
    return 0;
}
