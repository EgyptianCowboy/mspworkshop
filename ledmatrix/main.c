/**
 * xando1.c
 * Test program for the matrix display
 */

//#include <msp430.h>
#include <msp430g2553.h>

// Matrix display constants
#define MTXDIR P1DIR
#define MTXOUT P1OUT
#define MTXDATA BIT7
#define MTXCLK BIT5
#define MTXLATCH BIT4

// Function declarations
void initMtx(void);
void lightMtx(void);

//                          -B-  -G-  -R-
unsigned char pattern[24]={0xFF,0x00,0x00,
                           0x81,0x7E,0x00,
                           0x81,0x42,0x3C,
                           0x99,0x42,0x24,
                           0x99,0x42,0x24,
                           0x81,0x42,0x3C,
                           0x81,0x7E,0x00,
                           0xFF,0x00,0x00};

int main(void)
{
    volatile long x;
    volatile int y;
    unsigned char ch;
    
    WDTCTL = WDTPW | WDTHOLD; // stop watchdog timer
    
    initMtx();
    
    while(1)
    {
        lightMtx();
        for( x=0 ; x<30000 ; x++ ); // just loop delay
        
        for( y=0 ; y<8 ; y++ )
        {
            // Change blue LEDs to green pattern
            // Change green LEDs to red pattern
            // Change red LEDs to blue pattern
            
            ch = pattern[0+3*y]; // use ch as temporary value storage for later
            pattern[0+3*y] = pattern[1+3*y];
            pattern[1+3*y] = pattern[2+3*y];
            pattern[2+3*y] = ch;
        }
    }
    
    return 0;
}

// Function definitions

void initMtx(void)
{
    // Initialise pin that controls the Matrix Display
    MTXDIR |= MTXDATA + MTXCLK + MTXLATCH; // make outputs
    MTXOUT &= ~(MTXDATA + MTXCLK); // make low
    MTXOUT |= MTXLATCH; // LATCH is active low
}

void lightMtx(void)
{
    // Sends each byte in pattern buffer from byte 23 down to byte 0 to the matrix display module
    // Each byte is sent with the LSB first
    int x, by;
    unsigned char val;
    
    MTXOUT &= ~MTXLATCH; // make latch line low
    
    for( by=23 ; by>=0 ; by-- ) // for each byte in pattern buffer
    {
        val = pattern[by]; // load it into val
        
        for( x=8 ; x>0 ; x-- ) // for each bit in val shift it out
        {
            if( val&BIT0 )
            {
                MTXOUT |= MTXDATA; // make data line high
            }
            else
            {
                MTXOUT &= ~MTXDATA; // make data line low
            }
            
            MTXOUT |= MTXCLK; // make Clk line high
            MTXOUT &= ~MTXCLK; // make Clk line low
            val >>= 1;
        }
    }
    
    MTXOUT |= MTXLATCH; // make latch line high
}
