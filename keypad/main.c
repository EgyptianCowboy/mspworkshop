#include <msp430.h>
#include <stdint.h>
#include <stdbool.h>

#define ROW1 BIT0
#define ROW2 BIT1
#define ROW3 BIT2
#define ROW4 BIT3

#define COL1 BIT6
#define COL2 BIT5
#define COL3 BIT4

void initKeypad(void);
uint8_t scanKeypadDBC(void);
uint8_t scanKeypad(void);

int main() {
    WDTCTL = WDTPW | WDTHOLD;
    P1DIR |= 0x41;
    //P1DIR |= 0x01;
    initKeypad();
    while(1) {
        if(scanKeypadDBC() == 1) {
            P1OUT = 0x01;
        } else if(scanKeypadDBC() == 2){
            P1OUT = 0x40;
        } else {
            P1OUT = 0x00;
        }
    }
    //P1OUT = scanKeypadDBC() & 0x21;
    return 0;
}

void initKeypad(void) {
    P2SEL = 0x00;
    P2DIR = 0x00;
    P2OUT = 0x00;
    P2REN = 0xFF;
}

uint8_t scanKeypadDBC(void) {
    static uint8_t lastRead = 16;
    volatile uint8_t dCount;
    uint8_t val;

    val = scanKeypad();

    if(val != lastRead) {
        for (dCount = 0; dCount < 10; dCount++);

        val = scanKeypad();

        if(val != lastRead) {
            lastRead = val;
            return val;
        }
    }
}

uint8_t scanKeypad(void) {
    initKeypad();

    P2DIR |= COL3;
    P2REN &= ~COL3;
    P2OUT |= COL3;

    if(P2IN & ROW1) {
        return 3;
    }
    if(P2IN & ROW2) {
        return 6;
    }
    if(P2IN & ROW3) {
        return 9;
    }
    if(P2IN & ROW4) {
        return 12;
    }

    initKeypad();

    P2DIR |= COL2;
    P2REN &= ~COL2;
    P2OUT |= COL2;

    if(P2IN & ROW1) {
        return 2;
    }
    if(P2IN & ROW2) {
        return 5;
    }
    if(P2IN & ROW3) {
        return 8;
    }
    if(P2IN & ROW4) {
        return 11;
    }

    initKeypad();

    P2DIR |= COL1;
    P2REN &= ~COL1;
    P2OUT |= COL1;

    if(P2IN & ROW1) {
        return 1;
    }
    if(P2IN & ROW2) {
        return 4;
    }
    if(P2IN & ROW3) {
        return 7;
    }
    if(P2IN & ROW4) {
        return 10;
    }

    initKeypad();

    return 16;
}


